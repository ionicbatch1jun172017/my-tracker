export class Place {
    constructor(public name: string, public address: string, public latitude: number, public longtitude: number) {}
}