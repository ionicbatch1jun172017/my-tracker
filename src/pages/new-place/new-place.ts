import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from "../home/home";
import { PlaceService } from "../../service/place.service";
import { Place } from "../../model/new-place.model";

@IonicPage()
@Component({
  selector: 'page-new-place',
  templateUrl: 'new-place.html',
})
export class NewPlacePage {
  name: string;
  address: string;
  latitude: number;
  longtitude: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, private placeService: PlaceService) {}

  addNewPlace() {
    var newPlace = new Place(this.name, this.address, this.latitude, this.longtitude);
    this.placeService.addPlace(newPlace);

    this.navCtrl.push(HomePage);
  }
}
