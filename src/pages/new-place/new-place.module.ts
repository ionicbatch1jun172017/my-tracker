import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormsModule } from '@angular/forms';

import { NewPlacePage } from './new-place';

@NgModule({
  declarations: [
    NewPlacePage,
  ],
  imports: [
    FormsModule,
    IonicPageModule.forChild(NewPlacePage),
  ],
  exports: [
    NewPlacePage
  ]
})
export class NewPlacePageModule {}
